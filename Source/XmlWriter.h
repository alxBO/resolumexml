/*
  ==============================================================================

    XmlWriter.h
    Created: 24 Nov 2019 4:49:03pm
    Author:  alex

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "MappingReader.h"


class XmlWriter
{
public:
    XmlWriter(std::string fp);
    
    bool write(int num_tracks = 16, int num_columns = 128);
    
    void setVersion(std::map<std::string, std::string> version);
    
    void setMidiShortcutPreset(std::map<std::string, std::string> midi);
    
    void setMapping(std::vector<Mapper> map);
    
    bool getKey(int channel, int num, bool is_note, bool highres, String& out_st);
    
    
private:
    
    std::string fp;
    
    std::map<std::string, std::string> version;
    std::map<std::string, std::string> midi;
    std::vector<Mapper> map;
};
