/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "XmlReader.h"
#include "XmlWriter.h"
#include "MappingReader.h"

#include <thread>
#include <future>
#include <chrono>

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
	public juce::ActionListener,
	public juce::AsyncUpdater,
    public juce::TextButton::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

	void actionListenerCallback(const String& message)override;

	/** handleAsyncUpdate
	*/
	void handleAsyncUpdate()override;
private:
    //==============================================================================
    // Your private member variables go here...

    void buttonClicked (Button*)override;
    
    juce::ApplicationProperties app_properties;
    
    std::unique_ptr<juce::TextButton> m_open;
    std::unique_ptr<juce::TextButton> m_open_mapping;
    std::unique_ptr<juce::TextButton> m_export;
    std::unique_ptr<juce::Slider> m_num_tracks;
    std::unique_ptr<juce::Slider> m_num_columns;

    
	unsigned long long value;

    std::map<int, int> bytes;
    std::map<int, int> last_bytes;
    std::map<int, bool> bytes_modified;
    std::unique_ptr< ArenaMidiReader> arenaMidiReader;
    
    
    std::unique_ptr< MappingReader> mapping_reader;

    std::unique_ptr< XmlWriter> xml_writer;

	std::vector<std::future<void>> futures;

	static void linear_th(void* instance);
	std::atomic_bool should_stop;
	std::atomic<float> linear_value;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
