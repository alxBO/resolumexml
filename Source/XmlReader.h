#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <filesystem>
#include <atomic>
#include <thread>

class BaseXmlReader : public juce::AsyncUpdater
{
public:
	BaseXmlReader();
	~BaseXmlReader();

	/** open
	*/
	bool open(std::string xml_path);
    
    void EndThread();

    bool isOpened();

	/** handleAsyncUpdate
	* called when file has been modified
	*/
	void handleAsyncUpdate() = 0;

protected:
    std::string xml_path;
    std::unique_ptr<XmlDocument> xml_doc;


private:

	std::thread th;
	Time last_time;

	static int check_thread(void* instance, std::string fp);

	std::atomic<bool> should_finish, isRunning;
    
    bool opened;
};


// -------------------------


class ArenaMidiReader : public BaseXmlReader,
                        public juce::ActionBroadcaster
{
public:
    
    void handleAsyncUpdate()override;

    /** getRawMessage
    */
    std::map<int, int> getRawMessage() { return bytes; }

    /** getVersion
    */
    std::map<std::string, std::string>  getVersion(){return version;}
    
    /** getMidiShortcutPreset
    */
    std::map<std::string, std::string> getMidiShortcutPreset(){return midishorcutpreset;}
    
    unsigned long long getValue() { return value; }
    
private:

    // ---------
    unsigned long long value;
    std::map<int, int> bytes;
    std::map<std::string, std::string> version;
    std::map<std::string, std::string> midishorcutpreset;
};
