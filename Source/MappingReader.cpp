/*
  ==============================================================================

    MappingReader.cpp
    Created: 24 Nov 2019 7:29:57pm
    Author:  alex

  ==============================================================================
*/

#include "MappingReader.h"


using namespace std;


MappingReader::MappingReader()
{

}

MappingReader::~MappingReader()
{
}



// ----------


void MappingReader::handleAsyncUpdate()
{
    xml_doc = make_unique<XmlDocument>(File(xml_path));
    mapping.clear();
    
    auto mainElement = xml_doc->getDocumentElement();
    if(!mainElement)
    {
        String error = xml_doc->getLastParseError();
        cout << error << endl;
        return;
    }
        
    forEachXmlChildElement(*mainElement, entry)
    {
        Mapper m;
        m.path = entry->getStringAttribute("path").toStdString();
        m.is_note = entry->getBoolAttribute("is_note");
        m.highres = entry->getBoolAttribute("highres");
        m.ctrl_num = entry->getIntAttribute("ctrl_num");
//        m.channel = entry->getIntAttribute("channel");
        mapping.push_back(m);
    }
    
    // debug
    post();
    
    
    sendActionMessage("MAPPING_LOADED");
}

void MappingReader::post()
{
    cout << "------ MAPPING ------" << endl;
    for(auto m : mapping)
    {
        cout << "path : " << m.path << endl;
        cout << "ctrl : " << m.ctrl_num << endl;
        cout << "is_note : " << m.is_note << endl;
        cout << "highres : " << m.highres << endl;
        cout << endl;
        
    }
}
