/*
  ==============================================================================

    MappingReader.h
    Created: 24 Nov 2019 7:29:57pm
    Author:  alex

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <filesystem>
#include <atomic>
#include <thread>
#include "XmlReader.h"


struct Mapper
{
    std::string path;
    bool is_note;
    bool highres;
    int ctrl_num;
//    int channel;
    
};

class MappingReader :   public BaseXmlReader,
                        public juce::ActionBroadcaster
{
public:
    MappingReader();
    ~MappingReader();

    /** handleAsyncUpdate
    * called when file has been modified
    */
    void handleAsyncUpdate()override;

    /** getMapping
    */
    std::vector<Mapper>  getMapping(){return mapping;}

    /**
     */
    void post();
    
private:

    std::vector<Mapper> mapping;
};

