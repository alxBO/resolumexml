/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#ifdef WIN32
#include "Windows.h"
#include "Wincon.h"
#endif
using namespace std;
//==============================================================================
MainComponent::MainComponent()
{
#ifdef WIN32
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
#endif

    setSize (600, 400);

    PropertiesFile::Options options;
    options.applicationName = "ResolumeXML";
    options.filenameSuffix = ".settings";
    options.commonToAllUsers = true;
    options.osxLibrarySubFolder = "Application Support";
    options.folderName = "ResolumeXML";
    options.doNotSave = false;

    app_properties.setStorageParameters(options);
    

    // --------
	arenaMidiReader = make_unique<ArenaMidiReader>();
	arenaMidiReader->addActionListener(this);

    mapping_reader = make_unique<MappingReader>();
    mapping_reader->addActionListener(this);


	// ---------
	should_stop = false;
    
    // ---------
    
    m_open = make_unique<juce::TextButton>("open xml file");
    m_open->setBounds(50, 10, 150, 50);
    m_open->addListener(this);
    m_open->setColour(TextButton::ColourIds::textColourOffId, Colours::red);
    addAndMakeVisible(m_open.get());
    
    m_open_mapping = make_unique<juce::TextButton>("load mapping");
    m_open_mapping->setBounds(220, 10, 150, 50);
    m_open_mapping->addListener(this);
    m_open_mapping->setColour(TextButton::ColourIds::textColourOffId, Colours::red);
    addAndMakeVisible(m_open_mapping.get());
    
    m_export = make_unique<juce::TextButton>("export");
    m_export->setBounds(390, 10, 150, 50);
    m_export->addListener(this);
    addAndMakeVisible(m_export.get());
    
    m_num_tracks = make_unique<juce::Slider>("Num Tracks");
    m_num_tracks->setBounds(50, 80, 150, 20);
    m_num_tracks->setRange(1, 16, 1);
    m_num_tracks->setSliderStyle(juce::Slider::IncDecButtons);
    addAndMakeVisible(m_num_tracks.get());

    m_num_tracks->setValue(8);
    
    m_num_columns = make_unique<juce::Slider>("Num Columns");
    m_num_columns->setBounds(300, 80, 150, 20);
    m_num_columns->setRange(1, 128, 1);
    m_num_columns->setSliderStyle(juce::Slider::IncDecButtons);
    addAndMakeVisible(m_num_columns.get());
    
    m_num_columns->setValue(24);

    
    // ---------
    
    PropertiesFile* settings = app_properties.getUserSettings();
    String file_location = settings->getValue("file_location");
    if(file_location.isNotEmpty())
    {
        arenaMidiReader->open(file_location.toStdString());
        xml_writer = make_unique<XmlWriter>(file_location.toStdString());
        if(arenaMidiReader->isOpened())
            m_open->setColour(TextButton::ColourIds::textColourOffId, Colours::white);
        else
            m_open->setColour(TextButton::ColourIds::textColourOffId, Colours::red);
    }
    
    String mapping_location = settings->getValue("mapping_location");
    if(mapping_location.isNotEmpty())
    {
        mapping_reader->open(mapping_location.toStdString());
        if(mapping_reader->isOpened())
            m_open_mapping->setColour(TextButton::ColourIds::textColourOffId, Colours::white);
        else
            m_open_mapping->setColour(TextButton::ColourIds::textColourOffId, Colours::red);
    }
}

MainComponent::~MainComponent()
{
	should_stop = true;
	for (auto& f : futures)
	{
		f.get();
	}

    app_properties.saveIfNeeded();

}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setFont (Font (16.0f));
    g.setColour (Colours::white);

	g.drawText(String(value), 0, 0, getWidth(), getHeight()/2 + 10, Justification::centred);

	auto step = getWidth() / 17;


	// DECIMAL
	int i = 1;
	for (auto b : bytes)
	{
        g.setColour (Colours::white);

		if ((i % 4) == 0)
		{
			g.drawLine(step * (16 - b.first), getHeight() / 2, step * (16 - b.first), 50 + getHeight() / 2);
		}
        
        if(bytes_modified[b.first])
            g.setColour(juce::Colour::fromRGB(255, 0, 0));
        
		g.drawText(String(b.second), juce::Rectangle<float>(step * (16 - b.first), getHeight()/ 2, step, 50), Justification::centred);
		i++;
	}


	// HEX
	i = 1;
	const int OFFSET_Y = 50;
	for (auto b : bytes)
	{
        g.setColour (Colours::white);

		if ((i % 4) == 0)
		{
			g.drawLine(step * (16 - b.first), OFFSET_Y + getHeight() / 2, step * (16 - b.first), OFFSET_Y + 50 + getHeight() / 2);
		}
        
        if(bytes_modified[b.first])
            g.setColour(juce::Colour::fromRGB(255, 0, 0));


		g.drawText(String::toHexString(b.second), juce::Rectangle<float>(step * (16 - b.first), OFFSET_Y + getHeight() / 2, step, 50), Justification::centred);
		i++;
	}
	

	if (linear_value != 0)
	{
		g.setColour(juce::Colour::fromRGBA(255, 255, 255, 255 - 255.0f * (1.0f - linear_value)));
		g.fillRect(10, 10, 20, 20);

	}
}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    if(m_open)
    {
        m_open->setBounds(100, 10, 50, 20);

    }
}

void MainComponent::actionListenerCallback(const String & message)
{
	if (message.startsWith("XML_LOADED"))
	{
		bytes = arenaMidiReader->getRawMessage();
        if(last_bytes.empty())
            last_bytes = bytes;
        bytes_modified.clear();
        for(auto b : bytes)
        {
            if(b.second != last_bytes[b.first])
                bytes_modified[b.first] = true;
            else
                bytes_modified[b.first] = false;
        }
        last_bytes = bytes;
        
		value = arenaMidiReader->getValue();
		repaint();

		futures.push_back(std::async(linear_th, this));
	}
    if (message.startsWith("MAPPING_LOADED"))
    {
        if(xml_writer)
            xml_writer->setMapping(mapping_reader->getMapping());
    }
    
    
}

void MainComponent::handleAsyncUpdate()
{
	repaint();


}

void MainComponent::linear_th(void * instance)
{
	MainComponent* _this = (MainComponent*)instance;
	if (_this == NULL)
		return;

	_this->linear_value = 1.0f;

	while (_this->linear_value > 0 && !_this->should_stop)
	{
		_this->linear_value = _this->linear_value - 0.05f;
		_this->triggerAsyncUpdate();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

	}

}

void MainComponent::buttonClicked(juce::Button *button) {
    if(button == m_open.get())
    {
        FileChooser myChooser ("Select default.xml...",
                                File(),
                               "*.xml");

        if (myChooser.browseForFileToOpen())
        {
            File file (myChooser.getResult());

            auto path = file.getFullPathName().toStdString();
            PropertiesFile* settings = app_properties.getCommonSettings(true);
            settings->setValue("file_location", var(path));
            arenaMidiReader->open(path);
            if(arenaMidiReader->isOpened())
                m_open->setColour(TextButton::ColourIds::textColourOffId, Colours::white);
            else
                m_open->setColour(TextButton::ColourIds::textColourOffId, Colours::red);
            
            xml_writer = make_unique<XmlWriter>(path);

        }
    }
    else if (button == m_open_mapping.get())
    {
        FileChooser myChooser ("Select mapping.xml...",
                                File(),
                               "*.xml");

        if (myChooser.browseForFileToOpen())
        {
            File file (myChooser.getResult());

            auto path =file.getFullPathName().toStdString();
            PropertiesFile* settings = app_properties.getCommonSettings(true);
            settings->setValue("mapping_location", var(path));
            mapping_reader->open(path);
            if(mapping_reader->isOpened())
                m_open_mapping->setColour(TextButton::ColourIds::textColourOffId, Colours::white);
            else
                m_open_mapping->setColour(TextButton::ColourIds::textColourOffId, Colours::red);

        }
    }
    else if(button == m_export.get())
    {
        if(arenaMidiReader && xml_writer)
        {
            xml_writer->setVersion(arenaMidiReader->getVersion());
            xml_writer->setMidiShortcutPreset(arenaMidiReader->getMidiShortcutPreset());
            
            if(xml_writer->write(m_num_tracks->getValue(), m_num_columns->getValue()))
                AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::InfoIcon, "export", "export success");
            else
                AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, "export", "export error");

        }
        else
            AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, "export", "no xml loaded");
    }
    
}

