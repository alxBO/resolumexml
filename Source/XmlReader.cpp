#include "XmlReader.h"


using namespace std;


BaseXmlReader::BaseXmlReader():
	should_finish(false),
    isRunning(false)
{




}


BaseXmlReader::~BaseXmlReader()
{
    EndThread();

}

void BaseXmlReader::EndThread()
{
    should_finish = true;
    if(isRunning)
        if(th.joinable())
            th.join();

    isRunning = false;
    should_finish = false;
}
bool BaseXmlReader::open(std::string _xml_path)
{
	bool ret = false;
    opened = false;

	xml_path = _xml_path;

	if (File(xml_path).exists())
	{
        EndThread();
        
		th = std::thread(check_thread, this, xml_path);
		ret = true;
        opened = true;
	}
	else
	{
		cout << "unable to open : " << xml_path << endl;
	}

	return ret;
}

bool BaseXmlReader::isOpened()
{
    return opened;
}

int BaseXmlReader::check_thread(void* instance, std::string fp)
{
	BaseXmlReader* _this = (BaseXmlReader*)instance;
	if (_this == NULL)
		return -1;

	string filepath = fp;
    _this->isRunning = true;

	while (!_this->should_finish)
	{

		auto time = File(filepath).getLastModificationTime();

		if (_this->last_time != time)
		{
			_this->last_time = time;
            
			//notify
			_this->triggerAsyncUpdate();
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	return 0;
}


// ---------


void ArenaMidiReader::handleAsyncUpdate()
{
    xml_doc = make_unique<XmlDocument>(File(xml_path));
    bytes.clear();
    version.clear();
    value = 0;
    
    
    auto mainElement = xml_doc->getDocumentElement();
    if(!mainElement)
    {
        String error = xml_doc->getLastParseError();
        cout << error << endl;
        return;
    }
    
    
    auto num_attributes = mainElement->getNumAttributes();
    for(int i = 0; i < num_attributes; ++i)
    {
       midishorcutpreset[mainElement->getAttributeName(i).toStdString()] = mainElement->getAttributeValue(i).toStdString();
    }
    
    
    
    forEachXmlChildElementWithTagName(*mainElement, versioninfo, "versionInfo")
    {
        auto num_attributes = versioninfo->getNumAttributes();
        for(int i = 0; i < num_attributes; ++i)
        {
            version[versioninfo->getAttributeName(i).toStdString()] = versioninfo->getAttributeValue(i).toStdString();
        }
        
    }
    forEachXmlChildElementWithTagName(*mainElement, ShortcutManager, "ShortcutManager")
    {
        forEachXmlChildElementWithTagName(*ShortcutManager, Shortcut, "Shortcut")
        {
            forEachXmlChildElementWithTagName(*Shortcut, RawInputMessage, "RawInputMessage")
            {
                unsigned long long key = atoll( RawInputMessage->getStringAttribute("key").toStdString().c_str());
                value = key;
                for (int i = 0; i < 16; ++i)
                {
                    key = (value >> (i * 4)) & 0xF;
                    bytes[i] = (int)key;
                }
            }
        }
    }
    sendActionMessage("XML_LOADED");
}

