/*
  ==============================================================================

    XmlWriter.cpp
    Created: 24 Nov 2019 4:49:03pm
    Author:  alex

  ==============================================================================
*/

#include "XmlWriter.h"
using namespace std;


XmlWriter::XmlWriter(std::string _fp)
{
    fp = _fp;
}

void XmlWriter::setVersion(std::map<std::string, std::string> _version)
{
    version = _version;
}

void XmlWriter::setMidiShortcutPreset(std::map<std::string, std::string> _midi)
{
    midi = _midi;
}

void XmlWriter::setMapping(std::vector<Mapper> _map)
{
    map = _map;
}

// ---------------

bool XmlWriter::write(int num_tracks, int num_columns)
{
    XmlElement root ("MidiShortcutPreset");
    
    for(auto v : midi)
       root.setAttribute (String(v.first), v.second);
    
    // versionInfo
    XmlElement* versioninfo = new XmlElement ("versionInfo");
    for(auto v : version)
       versioninfo->setAttribute (String(v.first), v.second);
    root.addChildElement (versioninfo);
    
    // ShortcutManager
    XmlElement* ShortcutManager = new XmlElement ("ShortcutManager");
    ShortcutManager->setAttribute ("name", "MIDIShortcutManagerShortcuts");
    
    
    
    for(int i = 0; i < num_tracks; ++i)
    {
        for(auto m : map)
        {
            String path_tmp = m.path;
            path_tmp = path_tmp.replace("$", String(i+1));

            
            int num = path_tmp.contains("+") ? num_columns : 1;
            
            for(int j = 0; j < num ; ++j)
            {
                bool is_column = false;
                
                String path = path_tmp;
                if(path_tmp.contains("+"))
                {
                    is_column = true;
                    path = path_tmp.replace("+", String(j + 1));
                }

                String behaviour = m.is_note ? "1028" : "8";
                String paramNodeName = m.is_note ? "ParamEvent" : "ParamRange";
                String key;
                
                int ctrl_number = j + m.ctrl_num;   //ROOT KEY
                if(ctrl_number > 127)
                    break;
                
                
                if(getKey(i, is_column ? ctrl_number : m.ctrl_num, m.is_note, m.highres, key))
                {
                    // shortcut
                    XmlElement* Shortcut = new XmlElement ("Shortcut");
                    Shortcut->setAttribute("name", "Shortcut");
                    Shortcut->setAttribute("uniqueId", "1574611888942");
                    Shortcut->setAttribute("paramNodeName", "ParamRange");
                    Shortcut->setAttribute("behaviour", behaviour);
                    Shortcut->setAttribute("outputDeviceName", "No Output");

                    XmlElement* ShortcutPath_IN = new XmlElement ("ShortcutPath");
                    ShortcutPath_IN->setAttribute("name", "InputPath");
                    ShortcutPath_IN->setAttribute("path", path);
                    ShortcutPath_IN->setAttribute("translationType", "1");
                    ShortcutPath_IN->setAttribute("allowedTranslationTypes", "11");

                    XmlElement* ShortcutPath_OUT = new XmlElement ("ShortcutPath");
                    ShortcutPath_OUT->setAttribute("name", "OutputPath");
                    ShortcutPath_OUT->setAttribute("path", path);
                    ShortcutPath_OUT->setAttribute("translationType", "1");
                    ShortcutPath_OUT->setAttribute("allowedTranslationTypes", "11");
                   
                    XmlElement* Subtarget;
                    XmlElement* NamedValues;
                    if(!m.is_note)
                    {
                        Subtarget = new XmlElement ("Subtarget");
                        Subtarget->setAttribute("type", "5");
                        Subtarget->setAttribute("optionIndex", "-1");
                    }
                    else
                    {
                        NamedValues= new XmlElement ("NamedValues");
                        XmlElement * Value1 = new XmlElement ("Value");
                        Value1->setAttribute("first", "Connected");
                        Value1->setAttribute("second", "0.75");

                        XmlElement * Value2 = new XmlElement ("Value");
                        Value2->setAttribute("first", "Connected &amp; previewing");
                        Value2->setAttribute("second", "1");
                        
                        XmlElement * Value3 = new XmlElement ("Value");
                        Value3->setAttribute("first", "Disconnected");
                        Value3->setAttribute("second", "0.25");
                        
                        XmlElement * Value4 = new XmlElement ("Value");
                        Value4->setAttribute("first", "Empty");
                        Value4->setAttribute("second", "0");
                        
                        XmlElement * Value5 = new XmlElement ("Value");
                        Value5->setAttribute("first", "Previewing");
                        Value5->setAttribute("second", "0.5");

                        
                        NamedValues->addChildElement(Value1);
                        NamedValues->addChildElement(Value2);
                        NamedValues->addChildElement(Value3);
                        NamedValues->addChildElement(Value4);
                        NamedValues->addChildElement(Value5);
                    }


                    XmlElement* RawInputMessage = new XmlElement ("RawInputMessage");
                    RawInputMessage->setAttribute("name", "RawInputMessage");
                    RawInputMessage->setAttribute("key", key);
                    RawInputMessage->setAttribute("value", "0");
                    RawInputMessage->setAttribute("numSteps", m.highres ? "16384" : "128");


                    Shortcut->addChildElement(ShortcutPath_IN);
                    Shortcut->addChildElement(ShortcutPath_OUT);
                    if(!m.is_note)
                        Shortcut->addChildElement(Subtarget);
                    else
                        Shortcut->addChildElement(NamedValues);

                    Shortcut->addChildElement(RawInputMessage);
                    ShortcutManager->addChildElement(Shortcut);
                }
                else
                {
                   
                }
            }
            
            
        }
       
        
    }
    
    root.addChildElement (ShortcutManager);
    
    auto new_file = File(fp).getParentDirectory().getFullPathName() + "/Default.xml";
    
    bool ret = false;
    if( root.writeTo(new_file))
    {
        ret = true;
    }
        
//    cout <<  root.getText() << endl;
    
    return ret;
}


bool XmlWriter::getKey(int channel, int num, bool is_note, bool highres, String& out_st)
{
    if(channel< 0 ||  channel > 16)
        return false;
    
    if(num< 0 ||  num > 127)
        return false;
    
    if(is_note)
        highres = false;
    
    if(highres && num > 31)
        return false;
    
    unsigned long long channel_code = channel & 0xF;
    unsigned long long note_code = is_note ? 0x9 : 0xb;
    unsigned long long num_code = num & 0xFF;
    unsigned long long highres_code = highres ? 0x20 + num_code : 0x00;
    unsigned long long dummy_code = 0x00;
    unsigned long long type_code = is_note ? 0x01 : 0x03;
    
    unsigned long long  value = channel_code +
            (note_code << (1 * 4)) +
            (num_code << (2 * 4)) +
            (highres_code << (4 * 4)) +
            (dummy_code << (6 * 4)) +
            (dummy_code << (8 * 4)) +
            (dummy_code << (10 * 4)) +
            (dummy_code << (12 * 4)) +
            (type_code << (14 * 4));
    
    out_st = String(value);
    
    return true;
}
